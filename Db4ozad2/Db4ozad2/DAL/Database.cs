﻿using System.Collections.Generic;
using System.Linq;
using Db4objects.Db4o;
using Db4objects.Db4o.Config;
using Db4ozad2.Models;

namespace Db4ozad2.DAL
{
    public static class Database
    {
        private static bool isInstantianted;
        private static IEmbeddedConfiguration _config;
        public static IObjectContainer _db { get; private set; }

        public static void AccessDatabase(string fileName)
        {
            if (isInstantianted) return;
            fileName = fileName + ".db4o";
            _config = Db4oEmbedded.NewConfiguration();
            _config.Common.ObjectClass(typeof(Person)).CascadeOnUpdate(true);
            _config.Common.ObjectClass(typeof(Person)).CascadeOnDelete(false);
            _db = Db4oEmbedded.OpenFile(_config, fileName);
            isInstantianted = true;
        }

        public static void CloseDatabase()
        {
            _db.Close();
        }

        public static IList<Person> GetAllPeopleList()
        {
            return _db.Query<Person>();
        }

        public static Person GetPerson(string name)
        {
            return _db.Query<Person>(x => x.Name == name).FirstOrDefault();
        }

        public static void AddPerson(Person person)
        {
            _db.Store(person);
            _db.Commit();
        }

        public static void DeletePerson(Person person)
        {
            _db.Delete(person);
            _db.Commit();
        }

        public static void UpdatePerson(Person person)
        {
            _db.Store(person);
            _db.Commit();
        }
    }
}