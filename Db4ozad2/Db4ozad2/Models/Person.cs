﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Db4ozad2.Enums;

namespace Db4ozad2.Models
{
    public class Person : INotifyPropertyChanged
    {
        public string Name { get; set; }
        public DateTime? Birth { get; set; }
        public DateTime? Death { get; set; }
        public Person Mother { get; set; }
        public Person Father { get; set; }
        public eSex? Sex { get; set; }
        public List<Person> Children { get; set; } = new List<Person>();
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}