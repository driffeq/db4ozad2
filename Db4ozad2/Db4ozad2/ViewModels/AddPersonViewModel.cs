﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Caliburn.Micro;
using Db4ozad2.DAL;
using Db4ozad2.Enums;
using Db4ozad2.Models;
using Db4ozad2.Tools;

namespace Db4ozad2.ViewModels
{
    public class AddPersonViewModel : Screen
    {
        private readonly WindowManager _windowManager;
        public Array ComboBoxItemSource { get; set; }
        public Person PersonToAdd { get; set; } = new Person();
        public AddPersonViewModel()
        {
            _windowManager = new WindowManager();
            ComboBoxItemSource = Enum.GetValues(typeof(eSex));
        }

        public void RedirectToMainPage()
        {
            _windowManager.ShowWindow(new MainWindowViewModel());
            TryClose();
        }

        public void CheckName(TextBox tb)
        {
            if (tb.Text == String.Empty)
            {
                PersonToAdd.Name = String.Empty;
                return;
            }
            if (Validator.CheckName(tb.Text))
            {
                PersonToAdd.Name = tb.Text;
            }
            else
            {
                MessageBox.Show("Osoba o podanej nazwie już istnieje w bazie", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                tb.Text = String.Empty;
            }
        }

        public void CheckBirthDate(DatePicker dp)
        {
            if (dp.SelectedDate == null) return;
            if (Validator.CheckBirthDate(PersonToAdd, dp.SelectedDate))
            {
                PersonToAdd.Birth = dp.SelectedDate;
            }
            else
            {
                MessageBox.Show("Wybrana data nie spełnia założeń", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                dp.SelectedDate = null;
            }
        }
        public void CheckDeathDate(DatePicker dp)
        {
            if (dp.SelectedDate == null) return;
            if (Validator.CheckDeathDate(PersonToAdd, dp.SelectedDate))
            {
                PersonToAdd.Death = dp.SelectedDate;
            }
            else
            {
                MessageBox.Show("Wybrana data nie spełnia założeń", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                dp.SelectedDate = null;
            }
        }

        public void CheckParentMother(TextBox tb)
        {
            if (tb.Text.Equals(String.Empty)) return;
            var parent = Database.GetPerson(tb.Text);
            if (parent == null)
            {
                MessageBox.Show("Podana osoba nie istnieje", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                tb.Text = String.Empty;
                return;
            }
            if (Validator.CheckParentMother(PersonToAdd, parent, tb.Text))
            {
                PersonToAdd.Mother = parent;
            }
            else
            {
                MessageBox.Show("Podana osoba nie może być matką według założeń", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                tb.Text = String.Empty;
            }
        }

        public void CheckParentFather(TextBox tb)
        {
            if (tb.Text.Equals(String.Empty)) return;
            var parent = Database.GetPerson(tb.Text);
            if (parent == null)
            {
                MessageBox.Show("Podana osoba nie istnieje", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                tb.Text = String.Empty;
                return;
            }
            if (Validator.CheckParentFather(PersonToAdd, parent, tb.Text))
            {
                PersonToAdd.Father = parent;
            }
            else
            {
                MessageBox.Show("Podana osoba nie może być ojcem według założeń", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                tb.Text = String.Empty;
            }
        }

        public void AddPerson()
        {
            if (string.IsNullOrEmpty(PersonToAdd.Name))
            {
                MessageBox.Show("Osoba musi mieć nazwę", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                if (PersonToAdd.Mother!=null)
                {
                    PersonToAdd.Mother.Children.Add(PersonToAdd);
                }

                if (PersonToAdd.Father!=null)
                {
                    PersonToAdd.Father.Children.Add(PersonToAdd);
                }

                if (Validator.IsCycle(PersonToAdd, new HashSet<Person>()))
                {
                    MessageBox.Show("Cykl", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    Database.UpdatePerson(PersonToAdd.Mother);
                    Database.UpdatePerson(PersonToAdd.Father);
                    Database.AddPerson(PersonToAdd);
                    RedirectToMainPage();
                }
            }
        }
        //public void CheckParent(TextBox tb)
        //{
        //    if (tb.Text.Equals(String.Empty)) return;
        //    var parent = Database.GetPerson(tb.Text);
        //    if (parent == null)
        //    {
        //        tb.Text = String.Empty;
        //        MessageBox.Show("Podana osoba nie istnieje", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
        //        return;
        //    }
        //    if (tb.Name == "MotherName")
        //    {
        //        if (Validator.ValidateParent(PersonToAdd, parent, eSex.Female))
        //        {
        //            PersonToAdd.Mother = parent;
        //        }
        //        else
        //        {
        //            tb.Text = String.Empty;
        //        }
        //    }
        //    else if (tb.Name == "FatherName")
        //    {
        //        if (Validator.ValidateParent(PersonToAdd, parent, eSex.Male))
        //        {
        //            PersonToAdd.Father = parent;
        //        }
        //        else
        //        {
        //            tb.Text = String.Empty;
        //        }
        //    }

        //}
    }
}