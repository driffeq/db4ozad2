﻿using Caliburn.Micro;

namespace Db4ozad2.ViewModels
{
    public class DeletePersonViewModel : Screen
    {
        private readonly WindowManager _windowManager;

        public DeletePersonViewModel()
        {
            _windowManager = new WindowManager();

        }
        public void RedirectToMainPage()
        {
            _windowManager.ShowWindow(new MainWindowViewModel());
            TryClose();
        }
    }
}