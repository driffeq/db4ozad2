﻿using System.Collections.Generic;
using Caliburn.Micro;
using Db4ozad2.Models;

namespace Db4ozad2.ViewModels
{
    public class CommonAncestorViewModel : Screen
    {
        private readonly WindowManager _windowManager;
        public IEnumerable<Person> CommonAncestor { get; set; }

        public CommonAncestorViewModel(IEnumerable<Person> commonAncestor)
        {
            _windowManager = new WindowManager();
            CommonAncestor = commonAncestor;
        }
        public void RedirectToMainPage()
        {
            _windowManager.ShowWindow(new MainWindowViewModel());
            TryClose();
        }
    }
}