﻿using System.Collections.Generic;
using Caliburn.Micro;
using Db4ozad2.Models;
using Db4ozad2.Tools;

namespace Db4ozad2.ViewModels
{
    public class HeirViewModel : Screen
    {
        private readonly WindowManager _windowManager;
        public Person PersonToShowHeirs { get; set; }
        public IEnumerable<Person> Heirs { get; set; }

        public HeirViewModel(Person person)
        {
            _windowManager = new WindowManager();
            PersonToShowHeirs = person;
            Heirs = Services.FindHeirs(PersonToShowHeirs);

        }
        public void RedirectToMainPage()
        {
            _windowManager.ShowWindow(new MainWindowViewModel());
            TryClose();
        }
    }
}