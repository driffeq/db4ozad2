﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Caliburn.Micro;
using Db4ozad2.DAL;
using Db4ozad2.Enums;
using Db4ozad2.Models;
using Db4ozad2.Tools;
using Db4ozad2.Views;

namespace Db4ozad2.ViewModels
{
    public class MainWindowViewModel : Screen
    {
        private readonly WindowManager _windowManager;
        public string PersonNameToUpdate { get; set; }
        public string PersonNameToDelete { get; set; }
        public string PersonNameToShowFamilyTree { get; set; }
        public string PersonNameToShowHeirs { get; set; }
        public string PersonNameToShowAncestorFirst { get; set; }
        public string PersonNameToShowAncestorSecond { get; set; }

        public MainWindowViewModel()
        {
            _windowManager = new WindowManager();
            Database.AccessDatabase("db");
        }

        public void RedirectToAddPerson()
        {
            _windowManager.ShowWindow(new AddPersonViewModel());
            TryClose();
        }
        public void RedirectToEditPerson()
        {
            var person = Database.GetPerson(PersonNameToUpdate);
            if (person!=null)
            {
                _windowManager.ShowWindow(new EditPersonViewModel(person));
                TryClose();
            }
            else
            {
                MessageBox.Show("Podana osoba nie istnieje", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }
        public void RedirectToDeletePerson()
        {
            var person = Database.GetPerson(PersonNameToDelete);
            if (person!=null)
            {
                person.Mother?.Children.Remove(person);
                person.Father?.Children.Remove(person);
                foreach (var child in person.Children)
                {
                    if (person.Sex==eSex.Male)
                    {
                        child.Father = null;
                    }
                    else
                    {
                        child.Mother = null;
                    }
                }

                if (person.Mother!=null)
                {
                    Database.UpdatePerson(person.Mother);
                }

                if (person.Father!=null)
                {
                    Database.UpdatePerson(person.Father);
                }
                Database.UpdatePerson(person);
                Database.DeletePerson(person);
                MessageBox.Show("Podana osoba została usunięta", "OK", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("Podana osoba nie istnieje", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        public void RedirectToShowFamilyTree()
        {
            var person = Database.GetPerson(PersonNameToShowFamilyTree);
            if (person!=null)
            {
                _windowManager.ShowWindow(new FamilyTreeViewModel(person.Name));
                TryClose();
            }
            else
            {
                MessageBox.Show("Podana osoba nie istnieje", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        public void RedirectToShowHeirs()
        {
            var person = Database.GetPerson(PersonNameToShowHeirs);
            if (person!=null)
            {
                if (person.Children.Any())
                {
                    _windowManager.ShowWindow(new HeirViewModel(person));
                    TryClose();
                }
                else
                {
                    MessageBox.Show("Podana osoba nie ma dzieci", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            else
            {
                MessageBox.Show("Podana osoba nie istnieje", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        public void RedirectToShowCommonAncestor()
        {
            var person1 = Database.GetPerson(PersonNameToShowAncestorFirst);
            var person2 = Database.GetPerson(PersonNameToShowAncestorSecond);
            if (person1 != null && person2 != null)
            {
                _windowManager.ShowWindow(new CommonAncestorViewModel(Services.Ancestors(person1).Intersect(Services.Ancestors(person2))));
                TryClose();
            }
            else
            {
                MessageBox.Show("Podana osoba nie istnieje", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}