﻿using System.Collections.Generic;
using Caliburn.Micro;
using Db4ozad2.DAL;
using Db4ozad2.Models;

namespace Db4ozad2.ViewModels
{
    public class FamilyTreeViewModel : Screen
    {
        private readonly WindowManager _windowManager;
        public List<Person> PersonToShowFamilyTree { get; set; } = new List<Person>();

        public FamilyTreeViewModel(string personName)
        {
            _windowManager = new WindowManager();
            var person = Database.GetPerson(personName);
            PersonToShowFamilyTree.Add(person);
        }
        public void RedirectToMainPage()
        {
            _windowManager.ShowWindow(new MainWindowViewModel());
            TryClose();
        }
    }
}