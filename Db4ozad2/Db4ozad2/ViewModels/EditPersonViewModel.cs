﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Caliburn.Micro;
using Db4ozad2.DAL;
using Db4ozad2.Enums;
using Db4ozad2.Models;
using Db4ozad2.Tools;

namespace Db4ozad2.ViewModels
{
    public class EditPersonViewModel : Screen
    {
        private readonly WindowManager _windowManager;
        public Person PersonToEdit { get; set; }
        private string _personToEditName { get; set; }
        private string _personToEditMotherName { get; set; }
        private string _personToEditFatherName { get; set; }
        public Array ComboBoxItemSource { get; set; }


        public EditPersonViewModel(Person personToEdit)
        {
            _windowManager = new WindowManager();
            ComboBoxItemSource = Enum.GetValues(typeof(eSex));
            _personToEditName = personToEdit.Name;
            _personToEditMotherName = personToEdit.Mother?.Name;
            _personToEditFatherName = personToEdit.Father?.Name;
            PersonToEdit = Database.GetPerson(personToEdit.Name);
        }
        public void RedirectToMainPage()
        {
            _windowManager.ShowWindow(new MainWindowViewModel());
            TryClose();
        }

        public void CheckName(TextBox tb)
        {
            if (tb.Text == String.Empty)
            {
                PersonToEdit.Name = String.Empty;
                return;
            }

            if (tb.Text == _personToEditName) return;
            if (Validator.CheckName(tb.Text))
            {
                PersonToEdit.Name = tb.Text;
            }
            else
            {
                MessageBox.Show("Osoba o podanej nazwie już istnieje w bazie", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                tb.Text = String.Empty;
            }
        }
        public void CheckBirthDate(DatePicker dp)
        {
            if (dp.SelectedDate == null) return;
            if (Validator.CheckBirthDate(PersonToEdit, dp.SelectedDate))
            {
                PersonToEdit.Birth = dp.SelectedDate;
            }
            else
            {
                MessageBox.Show("Wybrana data nie spełnia założeń", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                dp.SelectedDate = null;
            }
        }
        public void CheckDeathDate(DatePicker dp)
        {
            if (dp.SelectedDate == null)
            {
                PersonToEdit.Death = null;
                return;
            }
            if (Validator.CheckDeathDate(PersonToEdit, dp.SelectedDate))
            {
                PersonToEdit.Death = dp.SelectedDate;
            }
            else
            {
                MessageBox.Show("Wybrana data nie spełnia założeń", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                dp.SelectedDate = null;
            }
        }
        public void CheckParentMother(TextBox tb)
        {
            if (tb.Text.Equals(String.Empty))
            {
                if (PersonToEdit.Mother!=null)
                {
                    var person = Database.GetPerson(PersonToEdit.Mother.Name);
                    try
                    {
                        person.Children.Remove(PersonToEdit);
                    }
                    catch (Exception e)
                    {

                    }
                    PersonToEdit.Mother = null;
                }
                return;
            }
            var parent = Database.GetPerson(tb.Text);
            if (parent == null)
            {
                MessageBox.Show("Podana osoba nie istnieje", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                tb.Text = String.Empty;
                return;
            }
            if (Validator.CheckParentMother(PersonToEdit, parent, tb.Text))
            {
                PersonToEdit.Mother = parent;
            }
            else
            {
                MessageBox.Show("Podana osoba nie może być matką według założeń", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                tb.Text = String.Empty;
            }
        }
        public void CheckParentFather(TextBox tb)
        {
            if (tb.Text.Equals(String.Empty))
            {
                if (PersonToEdit.Father != null)
                {
                    var person = Database.GetPerson(PersonToEdit.Father.Name);
                    try
                    {
                        person.Children.Remove(PersonToEdit);
                    }
                    catch (Exception e)
                    {

                    }
                    PersonToEdit.Father = null;
                }
                return;
            }
            var parent = Database.GetPerson(tb.Text);
            if (parent == null)
            {
                MessageBox.Show("Podana osoba nie istnieje", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                tb.Text = String.Empty;
                return;
            }
            if (Validator.CheckParentFather(PersonToEdit, parent, tb.Text))
            {
                PersonToEdit.Father = parent;
            }
            else
            {
                MessageBox.Show("Podana osoba nie może być ojcem według założeń", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                tb.Text = String.Empty;
            }
        }

        public void EditPerson()
        {
            if (string.IsNullOrEmpty(PersonToEdit.Name))
            {
                MessageBox.Show("Osoba musi mieć nazwę", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                if (PersonToEdit.Mother != null && PersonToEdit.Mother.Name != _personToEditMotherName)
                {
                    PersonToEdit.Mother.Children.Add(PersonToEdit);
                }
               
                if (PersonToEdit.Father != null && PersonToEdit.Father.Name != _personToEditFatherName)
                {
                    PersonToEdit.Father.Children.Add(PersonToEdit);
                }

                if (!string.IsNullOrEmpty(_personToEditMotherName))
                {
                    if (PersonToEdit.Mother?.Name != _personToEditMotherName)
                    {
                        var oldMother = Database.GetPerson(_personToEditMotherName);
                        oldMother.Children.Remove(PersonToEdit);
                        Database.UpdatePerson(oldMother);
                    }
                }
                if (!string.IsNullOrEmpty(_personToEditFatherName))
                {
                    if (PersonToEdit.Father?.Name != _personToEditFatherName)
                    {
                        var oldFather = Database.GetPerson(_personToEditFatherName);
                        oldFather.Children.Remove(PersonToEdit);
                        Database.UpdatePerson(oldFather);
                    }
                }
                if (Validator.IsCycle(PersonToEdit, new HashSet<Person>()))
                {
                    MessageBox.Show("Cykl", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    Database.UpdatePerson(PersonToEdit.Mother);
                    Database.UpdatePerson(PersonToEdit.Father);
                    Database.AddPerson(PersonToEdit);
                    RedirectToMainPage();
                }
            }
        }
    }
}