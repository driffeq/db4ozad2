﻿using System.ComponentModel;

namespace Db4ozad2.Enums
{
    public enum eSex
    {
        [Description("Mężczyzna")]
        Male,
        [Description("Kobieta")]
        Female
    }
}