﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Db4ozad2.Models;

namespace Db4ozad2.Tools
{
    public static class Services
    {
        public static IEnumerable<Person> FindHeirs(Person p)
        {
            if (p.Children.Any())
            {
                return p.Children?.Select(x => x?.Death == null ? new HashSet<Person> { x } : FindHeirs(x)).Aggregate(Enumerable.Concat);
            }
            else
            {
                return new HashSet<Person>();
            }
        }

        public static IEnumerable<Person> Ancestors(Person p)
        {
            return Enumerable.Empty<Person>()
                .Concat(p.Father != null ? new[] { p.Father }.Concat(Ancestors(p.Father)) : new Person[0])
                .Concat(p.Mother != null ? new[] { p.Mother }.Concat(Ancestors(p.Mother)) : new Person[0]);
        }
    }
    
}