﻿using System;
using System.Collections.Generic;
using System.IO.Packaging;
using System.Windows;
using Db4ozad2.DAL;
using Db4ozad2.Enums;
using Db4ozad2.Models;

namespace Db4ozad2.Tools
{
    public static class Validator
    {
        public static bool CheckName(string name)
        {
            return Database.GetPerson(name) == null;
        }
        public static bool CheckBirthDate(Person person, DateTime? birthDate)
        {
            if (person.Mother?.Death != null && birthDate > person.Mother?.Death) return false;
            if (person.Mother?.Birth != null && person.Mother?.Birth.Value.AddYears(10) > birthDate) return false;
            if (person.Mother?.Birth != null && birthDate.Value.AddYears(-60) > person.Mother?.Birth) return false;
            if (person.Father?.Death != null && birthDate > person.Father?.Death.Value.AddDays(-270)) return false;
            if (person.Father?.Birth != null && person.Father?.Birth.Value.AddYears(12) > birthDate) return false;
            if (person.Father?.Birth != null && birthDate.Value.AddYears(-70) > person.Father?.Birth) return false;
            if (person.Death != null && birthDate > person.Death) return false;
            foreach (var child in person.Children)
            {
                CheckBirthDate(child, child.Birth);
            }
            return true;
        }

        public static bool CheckDeathDate(Person person, DateTime? deathDate)
        {
            return person.Birth == null || !(person.Birth > deathDate);
        }

        public static bool CheckParentMother(Person person, Person parent, string name)
        {
            if (parent.Sex != eSex.Female) return false;
            if (parent.Birth != null && person.Birth != null && parent.Birth.Value.AddYears(10) > person.Birth) return false;
            if (parent.Birth != null && person.Birth != null && person.Birth.Value.AddYears(-60) > parent.Birth) return false;
            if (parent.Death != null && person.Birth != null && person.Birth > parent.Death) return false;
            return true;
        }
        public static bool CheckParentFather(Person person, Person parent, string name)
        {
            if (parent.Sex != eSex.Male) return false;
            if (parent.Birth != null && person.Birth != null && parent.Birth.Value.AddYears(12) > person.Birth) return false;
            if (parent.Birth != null && person.Birth != null && person.Birth.Value.AddYears(-70) > parent.Birth) return false;
            if (parent.Death != null && person.Birth != null && person.Birth > parent.Death.Value.AddDays(-270)) return false;
            return true;
        }

        public static bool IsCycle(Person p, HashSet<Person> list)
        {
            if (list.Contains(p))
            {
                return true;
            }
            list.Add(p);

            if (p.Mother != null && IsCycle(p.Mother, list))
            {
                return true;
            }
            if (p.Father != null && IsCycle(p.Father, list))
            {
                return true;
            }

            return false;
        }



        //public static bool ValidateParent(Person child, Person parent, eSex sex)
        //{
        //    if (parent.Sex != sex)
        //    {
        //        MessageBox.Show("Rodzic nie jest odpowiedniej płci", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
        //        return false;
        //    }
        //    if (child.Birth!=null && parent.Birth!=null)
        //    {
        //        switch (parent.Sex)
        //        {
        //            case eSex.Female:
        //                if (parent.Birth.Value.AddYears(10) > child.Birth || child.Birth.Value.AddYears(-60) > parent.Birth)
        //                {
        //                    MessageBox.Show("Matka musi mieć między 10-60 lat w trakcie narodzin dziecka.", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
        //                    return false;
        //                }
        //                break;
        //            case eSex.Male:
        //                if (parent.Birth.Value.AddYears(12) > child.Birth || child.Birth.Value.AddYears(-70) > parent.Birth)
        //                {
        //                    MessageBox.Show("Ojciec musi mieć między 12-70 lat w trakcie narodzin dziecka.", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
        //                    return false;
        //                }
        //                break;
        //        }
        //    }

        //    if (child.Birth!=null && parent.Death!=null)
        //    {
        //        switch (parent.Sex)
        //        {
        //            case eSex.Female:
        //                if (child.Birth > parent.Death)
        //                {
        //                    MessageBox.Show("Data śmierci matki jest wcześniejsza niż data narodzin dziecka.", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
        //                    return false;
        //                }
        //                break;
        //            case eSex.Male:
        //                if (child.Birth>parent.Death.Value.AddDays(-270))
        //                {
        //                    MessageBox.Show("Data śmierci ojca jest niezgodna z założeniem.", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
        //                    return false;
        //                }
        //                break;
        //        }
        //    }
        //    return true;
        //}
    }
}