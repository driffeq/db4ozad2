﻿using System.Windows;
using Caliburn.Micro;
using Db4ozad2.DAL;
using Db4ozad2.ViewModels;

namespace Db4ozad2
{
    public class AppBootstrapper : BootstrapperBase
    {
        public AppBootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<MainWindowViewModel>();
        }
    }
}